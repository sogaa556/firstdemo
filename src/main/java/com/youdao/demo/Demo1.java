package com.youdao.demo;

/**
 * @BelongsProject: FirstDemo
 * @BelongsPackage: com.youdao.demo
 * @Author: hq
 * @CreateTime: 2021-04-23 13:12
 * @Description:
 */
public class Demo1 {

  private  int num1;
  private int num2;

  public int add(int num1,int num2) {
    return num1+num2;
  }

    public int multiply(int num1,int num2) {
        return num1*num2;
    }

    public int sub(int num1,int num2) {
        return num1-num2;
    }
}
